<!DOCTYPE html>
<html>
    <head>
        <title>Dia da Semana</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
        <script>
            var baseUrl = '{{ url() }}';
        </script>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <button id="week-button" class="btn btn-lg btn-primary">QUE DIA DA SEMANA É HOJE?</button>
                <div class="title"></div>
            </div>
        </div>

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.min.js"></script>
        <script>
            $(function(){
                $(document).on('click', '#week-button', function(e){
                    e.preventDefault();

                    $.get(baseUrl + "/api/v1/week", function(d){
                        var day = d.data.week;
                        $('.title').html('Hoje é <b>' + day + '</b>');
                    }, 'json');
                });
            });
        </script>
    </body>
</html>
