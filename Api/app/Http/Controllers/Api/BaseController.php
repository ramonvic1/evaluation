<?php namespace App\Http\Controllers\Api;


use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as IlluminateBaseController;

class BaseController extends IlluminateBaseController
{
    use DispatchesJobs, ValidatesRequests;

    /**
     * Generates a response with a 204 HTTP header and a given message.
     *
     * @return mixed
     */
    public function responseNoContent()
    {
        return $this->responseWithArray(null, 204);
    }


    /**
     * Generates a response with a 400 HTTP header and a given message.
     *
     * @param string $message
     * @param array $data
     * @return mixed
     */
    public function responseWrongArgs($message = 'Wrong Arguments', $data = array())
    {
        return $this->responseWithError($message, 400, $data);
    }

    /**
     * Generates a response with a 401 HTTP header and a given message.
     *
     * @param string $message
     * @param array $data
     * @return mixed
     */
    public function responseUnauthorized($message = 'Unauthorized', $data = array())
    {
        return $this->responseWithError($message, 401, $data);
    }

    /**
     * Generates a response with a 403 HTTP header and a given message.
     *
     * @param string $message
     * @param array $data
     * @return mixed
     */
    public function responseForbidden($message = 'Forbidden', $data = array())
    {
        return $this->responseWithError($message, 403, $data);
    }

    /**
     * Generates a response with a 404 HTTP header and a given message.
     *
     * @param string $message
     * @param array $data
     * @return mixed
     */
    public function responseNotFound($message = 'Resource Not Found', $data = array())
    {
        return $this->responseWithError($message, 404, $data);
    }

    /**
     * Generates a response with a 405 HTTP header and a given message.
     *
     * @param string $message
     * @param array $data
     * @return mixed
     */
    public function responseMethodNotAllowed($message = 'Method Not Allowed', $data = array())
    {
        return $this->responseWithError($message, 405, $data);
    }

    /**
     * Response for a duplicated Resource in database
     *
     * @param string $message
     * @param array $data
     * @return mixed
     */
    public function responseDuplicatedResource($message = "Duplicated Resource", $data = array())
    {
        return $this->responseWithError($message, 409, $data);
    }

    /**
     * Response for a Internal Error
     *
     * @param string $message
     * @param array $data
     * @return mixed
     */
    public function responseServerError($message = 'Internal Error', $data = array())
    {
        return $this->responseWithError($message, 500, $data);
    }

    /**
     * Response for errors
     *
     * @param string $message
     * @param string $errorCode
     * @param array $extraData
     * @return mixed
     */
    public function responseWithError($message, $errorCode, $extraData = [])
    {
        return $this->responseWithArray(array_merge([
            'message' => $message
        ], $extraData), $errorCode);
    }

    /**
     * Response for generics
     *
     * @param array $array
     * @param int $responseCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseWithArray( $array, $responseCode = 200)
    {
        return response($array, $responseCode);
    }

    /**
     * Response for generics
     *
     * @param $item
     * @param int $responseCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseWithItem( $item, $responseCode = 200)
    {
        return $this->responseWithArray($item, $responseCode);
    }
}