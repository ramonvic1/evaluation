<?php

namespace App\Http\Controllers\Api;


class HelperController extends BaseController
{
    private $_weekDays = [
        'Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado'
    ];

    public function showWeek()
    {
        $data = [];

        $day = date('w');

        $data['week'] = $this->_weekDays[$day];

        return $this->responseWithArray([
            'data' => $data
        ]);
    }
}