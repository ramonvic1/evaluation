<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

use App\Http\Requests;

class UserController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return $this->responseWithArray($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email',
                'phone' => 'required',
            ]);

            extract($request->all());
            /** @var string $name */
            /** @var string $email */
            /** @var string $phone */

            /** @var User $user */
            $user = User::where('email', $email)->first();
            if($user) {
                return $this->responseDuplicatedResource("Use outro email esse email já está em uso.");
            }

            $user = new User();
            $user->email = $email;
            $user->name = $name;
            $user->phone = preg_replace("/\D/", "", $phone);

            $user->save();

            return $this->responseWithItem($user, 201);

        } catch (\HttpResponseException $e) {
            return $this->responseServerError("Preencha os campos corretamente e tente novamente.", ['error' => $e->getResponse()->getContent()]);
        } catch (QueryException $e) {
            if ($e->getCode() == 23000 ) {
                return $this->responseDuplicatedResource("Já existe um usuário utilizando o endereço de emal dessa conta. Entre em contato com o Suporte");
            } else {
                return $this->responseServerError("Houve um erro ao processar sua requisição", ['error' => $e->getMessage()]);
            }
        } catch (\ErrorException $e) {
            return $this->responseServerError("Houve um erro ao processar sua requisição", ['error' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
