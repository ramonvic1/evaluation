<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;

class Model extends EloquentModel
{

    const DEFAULT_PAGE_SIZE = 20;

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * Find model by token column
     * @param $token
     * @return $this
     */
    public static function findByToken($token)
    {
        return self::where('token', $token)->first();
    }
}