<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('categories')->delete();

        DB::table('categories')->insert([
            [
                'name' => 'Restaurantes',
                'image' => '/images/icon-restaurante.png',
                'status' => 1,
            ],[
                'name' => 'Cafeteria',
                'image' => '/images/icon-cafeteria.png',
                'status' => 1,
            ],[
                'name' => 'Bar',
                'image' => '/images/icon-bar.png',
                'status' => 1,
            ],[
                'name' => 'Padaria',
                'image' => '/images/icon-padaria.png',
                'status' => 1,
            ]
        ]);

        $this->command->info("Categories table seeded :)");
    }
}
