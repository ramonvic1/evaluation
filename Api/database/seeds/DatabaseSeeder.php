<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        \Laracasts\TestDummy\Factory::$factoriesPath = 'database/factories';

        $this->call(UserTableSeeder::class);


        Model::reguard();
    }
}
