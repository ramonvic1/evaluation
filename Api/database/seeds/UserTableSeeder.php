<?php

use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
            [
                'name' => 'Ramon Vicente',
                'email' => 'ramonvic@me.com',
                'password' => bcrypt('1'),
                'remember_token' => str_random(10),
                'role'=>0,
                'status'=>1,
            ],
            [
                'name' => 'Henrique Andrade',
                'email' => 'henrique@cefis.com.br',
                'password' => bcrypt('1'),
                'remember_token' => str_random(10),
                'role'=>0,
                'status'=>1,
            ],
            [
                'name' => 'Ana Paula',
                'email' => 'ana@cefis.com.br',
                'password' => bcrypt('1'),
                'remember_token' => str_random(10),
                'role'=>0,
                'status'=>1,
            ]
        ]);

        $this->command->info("Users table seeded :)");
    }
}
