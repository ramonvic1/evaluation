<?php

use Illuminate\Database\Seeder;

class ApplicationKeyTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('application_keys')->delete();

        DB::table('application_keys')->insert(array(
            array(
                'client'=>'Mix de Ofertas',
                'key'=>'r2d2',
                'active'=> true,
                'created_at'=> \Carbon\Carbon::now()
            )
        ));

        $this->command->info("Application Keys table seeded :)");
    }
}