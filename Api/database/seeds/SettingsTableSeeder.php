<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class SettingsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('settings')->delete();

        DB::table('settings')->insert(array(
            array(
                'site_url' => 'http://www.mixdeofertas.com.br',
                'facebook_url' => 'https://www.facebook.com/mixdeofertas',
                'twitter_url' => 'https://twitter.com/mixdeofertas',
                'instagram_url' => 'http://instagram.com/mixdeofertas',
                'phone' => '(62) 9999-9999',
                'about'=>'<p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.</p>
                            <p>Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.</p>',
                'terms'=>'<p>Termos de Serviço do Google</p>
                        <p>Última modificação: 14 de abril de 2014 (visualizar versões arquivadas)</p>
                        <p>Bem-vindo ao Google!</p>
                        <p>Agradecemos por usar nossos produtos e serviços (“Serviços”). Os Serviços serão fornecidos pelo Google Inc. (“Google”), localizado em 1600 Amphitheatre Parkway, Mountain View, CA 94043, Estados Unidos.</p>
                        <p>Ao usar nossos Serviços, você está concordando com estes termos. Leia-os com atenção.</p>
                        <p>Nossos Serviços são muito diversos, portanto, às vezes, podem aplicar-se termos adicionais ou exigências de produtos (inclusive exigências de idade). Os termos adicionais estarão disponíveis com os Serviços relevantes e esses termos adicionais se tornarão parte de nosso contrato com você, caso você use esses Serviços.</p>
                        <p>Como usar nossos Serviços</p>
                        <p>É preciso que você siga as políticas disponibilizadas a você dentro dos Serviços.</p>
                        <p>Não faça uso indevido de nossos Serviços. Por exemplo, não interfira com nossos Serviços nem tente acessá-los por um método diferente da interface e das instruções que fornecemos. Você pode usar nossos serviços somente conforme permitido por lei, inclusive leis e regulamentos de controle de exportação e reexportação. Podemos suspender ou deixar de fornecer nossos Serviços se você descumprir nossos termos ou políticas ou se estivermos investigando casos de suspeita de má conduta.</p>
                        <p>O uso de nossos Serviços não lhe confere a propriedade sobre direitos de propriedade intelectual sobre os nossos Serviços ou sobre o conteúdo que você acessar. Você não pode usar conteúdos de nossos Serviços a menos que obtenha permissão do proprietário de tais conteúdos ou que o faça por algum meio permitido por lei. Estes termos não conferem a você o direito de usar quaisquer marcas ou logotipos utilizados em nossos Serviços. Não remova, oculte ou altere quaisquer avisos legais exibidos em ou junto a nossos Serviços.</p>
                        <p>Nossos Serviços exibem alguns conteúdos que não são do Google. Esses conteúdos são de exclusiva responsabilidade da entidade que os disponibiliza. Podemos revisar conteúdo para determinar se é ilegal ou se infringe nossas políticas, e podemos remover ou nos recusar a exibir conteúdos que razoavelmente acreditamos violar nossas políticas ou a lei. Mas isso não significa, necessariamente, que revisaremos conteúdos, portanto por favor, não presuma que o faremos.</p>
                        <p>Em relação com seu uso dos Serviços, podemos enviar-lhe anúncios de serviços, mensagens administrativas e outras informações. Você pode desativar algumas dessas comunicações.</p>
                        <p>Alguns dos nossos Serviços estão disponíveis em dispositivos móveis. O usuário não deve utilizar tais Serviços de forma que o distraia ou o impeça de cumprir leis de trânsito ou de segurança.</p>
                        <p>Sua Conta do Google</p>
                        <p>Talvez você precise criar uma Conta do Google para utilizar alguns dos nossos Serviços. Você poderá criar sua própria Conta do Google ou sua Conta do Google poderá ser atribuída a você por um administrador, como seu empregador ou instituição de ensino. Se você estiver usando uma Conta do Google atribuída a você por um administrador, termos diferentes ou adicionais podem aplicar-se e seu administrador poderá ser capaz de acessar ou desativar sua conta.</p>
                        <p>Para proteger sua Conta do Google, o usuário deve manter a senha em sigilo. A atividade realizada na Conta do Google ou por seu intermédio é de responsabilidade do usuário. Não recomendamos que a senha da Conta do Google seja reutilizada em aplicativos de terceiros. Caso tome conhecimento de uso não autorizado da sua senha ou Conta do Google, o usuário deve seguir estas instruções.</p>
                        <p>Proteção à Privacidade e aos Direitos Autorais</p>
                        <p>As Políticas de Privacidade do Google explicam o modo como tratamos seus dados pessoais e protegemos sua privacidade quando você usa nossos Serviços. Ao utilizar nossos Serviços, você concorda que o Google poderá usar esses dados de acordo com nossas políticas de privacidade.</p>
                        <p>Nós respondemos às notificações de alegação de violação de direitos autorais e encerramos contas de infratores reincidentes de acordo com os procedimentos estabelecidos na Lei de Direitos Autorais Digital do Milênio dos Estados Unidos (U.S. Digital Millennium Copyright Act).</p>
                        <p>Fornecemos informações para ajudar os detentores de direitos autorais a gerenciarem sua propriedade intelectual on-line. Caso você entenda que alguém está violando seus direitos autorais e quiser nos notificar, você pode encontrar informações sobre o envio de notificações e sobre a política do Google para respondê-las em nossa Central de Ajuda.</p>
                        <p>Seu Conteúdo em nossos Serviços</p>
                        <p>Alguns de nossos Serviços permitem que você faça upload, submeta, armazene, envie ou receba conteúdo. Você mantém a propriedade de quaisquer direitos de propriedade intelectual que você detenha sobre aquele conteúdo. Em resumo, aquilo que pertence a você, permanece com você.</p>
                        <p>Quando você faz upload, submete, armazena, envia ou recebe conteúdo a nossos Serviços ou por meio deles, você concede ao Google (e àqueles com quem trabalhamos) uma licença mundial para usar, hospedar, armazenar, reproduzir, modificar, criar obras derivadas (como aquelas resultantes de traduções, adaptações ou outras alterações que fazemos para que seu conteúdo funcione melhor com nossos Serviços), comunicar, publicar, executar e exibir publicamente e distribuir tal conteúdo. Os direitos que você concede nesta licença são para os fins restritos de operação, promoção e melhoria de nossos Serviços e de desenvolver novos Serviços. Essa licença perdura mesmo que você deixe de usar nossos Serviços (por exemplo, uma listagem de empresa que você adicionou ao Google Maps). Alguns Serviços podem oferecer-lhe modos de acessar e remover conteúdos que foram fornecidos para aquele Serviço. Além disso, em alguns de nossos Serviços, existem termos ou configurações que restringem o escopo de nosso uso do conteúdo enviado nesses Serviços. Certifique-se de que você tem os direitos necessários para nos conceder a licença de qualquer conteúdo que você enviar a nossos Serviços.</p>
                        <p>Nossos sistemas automatizados analisam o seu conteúdo (incluindo e-mails) para fornecer recursos de produtos pessoalmente relevantes para você, como resultados de pesquisa customizados, propagandas personalizadas e detecção de spam e malware. Essa análise ocorre à medida que o conteúdo é enviado e recebido, e quando ele é armazenado.</p>
                        <p>Se o usuário tiver uma Conta do Google, o nome e a foto do perfil, bem como as ações realizadas em aplicativos do Google ou de terceiros que estejam conectados a essa Conta do Google (como marcações +1, avaliações e comentários postados), poderão aparecer em nossos Serviços, inclusive para exibição em anúncios e em outros contextos comerciais. As opções do usuário para limitar as configurações de compartilhamento ou visibilidade na Conta do Google serão respeitadas. Por exemplo, o usuário pode alterar as configurações de modo que seu nome e foto não apareçam em anúncios.</p>
                        <p>Você pode encontrar mais informações sobre como o Google usa e armazena conteúdo na política de privacidade ou termos adicionais de Serviços específicos. Se você enviar comentários ou sugestões sobre nossos Serviços, podemos usar seus comentários ou sugestões sem obrigação para você.</p>
                        <p>Sobre Software em nossos Serviços</p>
                        <p>Quando um Serviço exige ou inclui software disponível para download, tal software poderá atualizar-se automaticamente em seu dispositivo se uma nova versão ou recurso estiver disponível. Alguns Serviços podem permitir que você ajuste suas configurações de atualizações automáticas.</p>
                        <p>O Google concede a você uma licença pessoal, mundial, não exclusiva, intransferível e isenta de royalties para o uso do software fornecido pelo Google como parte dos Serviços. Essa licença tem como único objetivo permitir que você use e aproveite o benefício dos Serviços, tal como fornecidos pelo Google, da forma permitida por estes termos. Você não poderá copiar, modificar, distribuir, vender ou alugar qualquer parte de nossos Serviços ou o software incluso, nem poderá fazer engenharia reversa ou tentar extrair o código fonte desse software, exceto nos casos em que a legislação proibir tais restrições, ou quando você tiver nossa permissão por escrito.</p>
                        <p>Software de código aberto é importante para nós. Alguns dos softwares usados em nossos Serviços podem ser oferecidos sob uma licença de código aberto que colocaremos a sua disposição. Pode haver disposições na licença de código aberto que substituam expressamente alguns desses termos.</p>',
                'created_at'=> \Carbon\Carbon::now(),
                'updated_at'=> \Carbon\Carbon::now()
            )
        ));

        $this->command->info("Settings table seeded :)");
    }
}
