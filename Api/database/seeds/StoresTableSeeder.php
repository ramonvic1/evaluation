<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class StoresTableSeeder extends Seeder
{
    public function run()
    {
        TestDummy::times(140)->create('App\Models\Store');
    }
}
