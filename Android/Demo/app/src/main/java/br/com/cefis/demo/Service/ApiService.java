package br.com.cefis.demo.Service;


import java.util.List;

import br.com.cefis.demo.Model.User;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;

public interface ApiService {

    @GET("users")
    Call<List<User>> listUsers();

    @POST("users")
    Call<User> newUser(@Body User user);
}
