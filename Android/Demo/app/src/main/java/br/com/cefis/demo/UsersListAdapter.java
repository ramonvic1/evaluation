package br.com.cefis.demo;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.cefis.demo.Model.User;

public class UsersListAdapter  extends RecyclerView.Adapter<UsersListHolder> {
    private ArrayList<User> mUsers = new ArrayList<>();

    private static final String TAG = "UsersListAdapter";

    public UsersListAdapter() {

    }

    public UsersListAdapter(ArrayList<User> users) {
        mUsers = users;
    }

    @Override
    public UsersListHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view, parent, false);

        UsersListHolder vh = new UsersListHolder(v);
        return vh;
    }

    public void setUsers(List<User> users) {
        this.mUsers = new ArrayList<>(users);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(UsersListHolder holder, int position) {
        User user = mUsers.get(position);
        holder.bindUser(user);
        Log.d(TAG, "binding user" + user + "at position" + position);
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }
}