package br.com.cefis.demo.Model;


public class User {

    private int id;
    private String name;
    private String email;
    private String phone;
    private String avatar;

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getAvatar() {
        return avatar;
    }
}
