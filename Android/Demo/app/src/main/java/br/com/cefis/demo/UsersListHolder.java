package br.com.cefis.demo;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

import br.com.cefis.demo.Model.User;

public class UsersListHolder extends RecyclerView.ViewHolder {

    private User mUser;
    private TextView nameTextView;
    private TextView emailTextView;

    public UsersListHolder(View itemView) {
        super(itemView);
        nameTextView = (TextView)itemView.findViewById(R.id.profile_name);
        emailTextView = (TextView)itemView.findViewById(R.id.profile_email);
    }

    public void bindUser(User user) {
        mUser = user;
        nameTextView.setText(user.getName());
        emailTextView.setText(user.getEmail());
    }
}
