package br.com.cefis.demo;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import br.com.cefis.demo.Service.ApiService;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class Application extends android.app.Application {

    private final static String baseUrl = "http://54.94.155.60/public/api/v1/";

    @Override
    public void onCreate() {
        super.onCreate();

    }


    public ApiService getApiService() {

        OkHttpClient client = new OkHttpClient();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        client.interceptors().add(interceptor);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(ApiService.class);
    }
}
